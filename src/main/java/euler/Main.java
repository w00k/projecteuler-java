package euler;

import org.joda.time.LocalTime;
import euler.common.*;

public class Main {
  public static void main(String[] args) {
	LocalTime currentTime = new LocalTime();
	System.out.println("The current local time is: " + currentTime);

	Problem016 answer = new Problem016();
	answer.showAnswer();
  }
}
