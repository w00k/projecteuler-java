package euler.common;

import java.math.BigInteger;

public class Problem010 {

	private BigInteger sumPrime;
	private Integer topNumber;

	public Problem010() {
		this.sumPrime = new BigInteger("0");
		this.topNumber = 2000000;
	}

	/**
	 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17. Find the sum of all the
	 * primes below two million.
	 */
	public void showAnswer() {
		boolean isNumberPrime = false;
		for (Integer i = 2; i < topNumber; i++) {
			isNumberPrime = isProbable(new BigInteger(i.toString()));
			if (isNumberPrime) {
				sumPrime = sumPrime.add(new BigInteger(i.toString()));
				// System.out.println("isPrime : " + i + " - sumPrime : " + sumPrime);
			}
		}
		// the result is 142913828922, number 1 is not considera
		System.out.println("The sum of all the primes below two million is : " + sumPrime);
	}

	private boolean isPrime(Integer number) {
		Integer auxNumber = number;
		Integer auxValue = 2;
		Integer middleAuxNumber = number / 2;

		if (auxNumber != 2 && auxNumber % 2 == 0)
			return false;

		while (middleAuxNumber > auxValue) {
			if (auxNumber % auxValue == 0)
				return false;
			auxValue++;
		}
		return true;
	} // end isPrime

	// implement of isProbablePrime natiev for Java
	private boolean isProbable(BigInteger number) {
		int certainty = 1000;
		return number.isProbablePrime(certainty);
	}

}
