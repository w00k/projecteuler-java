package euler.common;

import java.util.ArrayList;
import java.util.List;

public class Problem006 {

	public Problem006() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * The sum of the squares of the first ten natural numbers is, 1exp2 + 2exp2
	 * + ... + 10exp2 = 385 The square of the sum of the first ten natural
	 * numbers is, (1 + 2 + ... + 10)exp2 = 552 = 3025 Hence the difference
	 * between the sum of the squares of the first ten natural numbers and the
	 * square of the sum is 3025 − 385 = 2640. Find the difference between the
	 * sum of the squares of the first one hundred natural numbers and the
	 * square of the sum.
	 */
	public void showAnswer() {
		List<Integer> numbers = getNumber(100);
		Integer sumExp = 0, sumNumber = 0;
		
		for (Integer num : numbers) {
			sumExp = sumExp + expTwo(num);
			sumNumber = sumNumber + num;
		}
		
		sumNumber = expTwo(sumNumber);
		
		System.out.println("sumExp :  " + sumExp);
		System.out.println("sumNumber exp 2 : " + sumNumber);
		System.out.println("result: " + (sumNumber - sumExp));
	}

	private List<Integer> getNumber(Integer maxNumber) {
		List<Integer> numbers = new ArrayList<Integer>();
		for (Integer i = 1; i <= maxNumber; i++) {
			numbers.add(i);
		}
		return numbers;
	} // end getNumber
	
	private Integer expTwo(Integer number){
		return number * number;
	}

}
