package euler.common;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author w00kyx
 * 
 * The following iterative sequence is defined for the set of positive integers:
 * n → n/2 (n is even)
 * n → 3n + 1 (n is odd)
 * Using the rule above and starting with 13, we generate the following sequence:
 * 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
 * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
 * Which starting number, under one million, produces the longest chain?
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 */
public class Problem014 {
	
	private Integer topNumber, temp;
    
    public Problem014() {
		this.topNumber = 1000000;
		this.temp = 0;
	}

	public void showAnswer() {     
        Integer number = 0, count = 0;
        
        for(number = 2; number < topNumber; number++){
            count = longestChain(number);
            if(temp < count) {
                temp = count;
                //System.out.println("number : " + number + " count : " + count);
            } 
        } //end for
        System.out.println("euler.common.Problem014.showAnswer the longest chain is  " + temp);
    }
    
    private boolean isOdd(long number) {
        return number%2 != 0;
    }
    
    private long getEven(long number) {
        return (number/2);
    }
    
    private long getOdd(long number) {
        return (3*number + 1);
    }
    
    private Integer longestChain(Integer number) {
        long tempNumber = number.longValue();
        Integer count = 1;
        while(tempNumber > 1) {
            if(isOdd(tempNumber)) tempNumber = getOdd(tempNumber);
            else tempNumber = getEven(tempNumber);
            count++; 
        }
        return count;
    }
}
