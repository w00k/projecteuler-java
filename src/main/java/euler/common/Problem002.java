package euler.common;

/**
 * Even Fibonacci numbers
 * Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, 
 *  the first 10 terms will be: 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
 * By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.
 * */

public class Problem002 {

	private Integer result;
	private Integer exceed; 
	
	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}
	
	public Integer getExceed() {
		return exceed;
	}

	public void setExceed(Integer exceed) {
		this.exceed = exceed;
	}

	public void addResult(Integer number, boolean showLog) { 
		if(showLog) {
			System.out.println("even number : " + number );
		}
		setResult(getResult() + number);
	}
	
	public Problem002() {
		this.result = 0;
		this.exceed = 4000000;
	}
	
	public  void showAnswer() {
		exceed = 4000000;
		Integer aux = 0;
		for(int i = 0; i < exceed; i++) {
			aux = fibonacci(i);
			if(aux < exceed) {
				if((aux & 1) == 0) {
					addResult(aux, true);
				} else {
					System.err.println("ODD : " + aux);
				}
			} else { 
				System.out.println("end of the road ... " + getResult());
				break;
			}
		}
	}
	
	private int fibonacci(int n) {
		int value = 0;
	    if (n > 1) {
	    	value = fibonacci(n-1) + fibonacci(n-2);  //función recursiva
	    	return value;
	    } else if (n == 1) {  
	        return 1;
	    } else if (n == 0){  
	        return 0;
	    } else{ 
	        System.out.println("Debes ingresar un tamaño mayor o igual a 1");
	        return -1; 
	    }
	}
	
}
