package euler.common;

import java.math.BigInteger;

/**
 *
 * @author w00kx
 */
public class Problem016 {
    
    private BigInteger base = BigInteger.ZERO;
    
    public Problem016() {
        base = new BigInteger("2");
    }
    
    public void showAnswer() {
        String resp;
        int exp = 1000;
        resp = getExpo(exp);
        System.out.println(" 2 ^ " + exp + " = " + resp);
        resp = getSumOfDigit(resp);
        System.out.println("sum of digits : " + resp);
    }
    
    private String getExpo(int value) { 
        BigInteger resp = BigInteger.ONE;
        for(int i = 0; i < value; i++) { 
            resp = base.multiply(resp);
        }
        return resp.toString();
    }
    
    private String getSumOfDigit(String number) {
        int length = number.length();
        Integer resp = 0;
        char strDigit; 
        for(int i = 0 ; i < length; i++) {
            strDigit = number.charAt(i);
            resp = resp +  strDigit-'0';
        }
        return resp.toString(); 
    }
}
