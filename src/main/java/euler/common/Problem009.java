package euler.common;

public class Problem009 {

	private Integer a, b, c, topNumber;
	
	public Problem009() {
		topNumber = 1000;
	}

	/**
	 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
	 * a2 + b2 = c2
	 * For example, 3exp2 + 4exp2 = 9 + 16 = 25 = 5exp2.
	 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
	 * Find the product abc.
	 * */
	public void showAnswer() {
		for(c = topNumber; c > 3; c--) {
			for(b=(c-1); b > 2; b--) {
				for(a=(b-1); a > 1; a--) {
					
					if(isOneThousand() && isPythagorean()){
						System.out.println("a : " + a + "\nb : " + b + "\nc : " + c);
						System.out.println("a*b*c = " + (a*b*c));
					}
					
				} //end decrease c
			} //end decrease b
		} //end decrease a
	}
	
	private boolean isPythagorean() {
		boolean isPythagorean = false; 
		Integer auxA, auxB, auxC;
		if(a != b && b != c) {
			auxA = a * a;
			auxB = b * b;
			auxC = c * c;
			if((auxA + auxB) == auxC) {
				isPythagorean = true;
			}
		}
		return isPythagorean;
	}
	
	private boolean isOneThousand() {
		boolean oneThousand = false; 
		Integer auxA, auxB, auxC;
		
		if(a != b && b != c) {
			
			auxA =  a;
			auxB = b;
			auxC = c;
			
			if((auxA + auxB + auxC) == 1000) {
				oneThousand = true;
			}
		}		
		return oneThousand;
	}

}
