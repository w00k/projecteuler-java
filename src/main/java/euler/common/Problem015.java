/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package euler.common;

import java.math.BigInteger;

/**
 *
 * @author Francisco
 */
public class Problem015 {

	public void showAnswer() {
        System.out.println("euler.commonProblem015 . binominal(40,20) : " + 
                binomial(40, 20).toString());
    }

    public BigInteger binomial(int n, int k) {
        if (k < 0 || k > n) return BigInteger.ONE;
        BigInteger binominal = BigInteger.ONE;
        for (int i = 0; i < k; i++) {
            binominal = binominal.multiply(BigInteger.valueOf(n - i));
        }
        return binominal.divide(factorial(k));
    }
    
    public BigInteger factorial(int n) {
        if (n < 0) return BigInteger.ONE;
        BigInteger factorial = BigInteger.ONE;
        for (int i = 2; i <= n; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }
        return factorial;
    }
}
