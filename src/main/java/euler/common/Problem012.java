package euler.common;

public class Problem012 {

   private Integer topNumber;

	public Problem012() {
	   topNumber = 9000000;
	}

	/**
	 * The sequence of triangle numbers is generated by adding the natural numbers. So the 7th triangle number would be
	 * 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28. The first ten terms would be:
	 * 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
	 * Let us list the factors of the first seven triangle numbers:
	 *	 1: 1
	 *	 3: 1,3
	 *	 6: 1,2,3,6
	 *	10: 1,2,5,10
	 *	15: 1,3,5,15
	 *	21: 1,3,7,21
	 *	28: 1,2,4,7,14,28
	 * We can see that 28 is the first triangle number to have over five divisors.
	 * What is the value of the first triangle number to have over five hundred divisors? 76576500
         * 
         * Function x sub n = n(n+1)/2 ; n = 1 to N
         * 
         * this solution take more than 17 minutes
	 * */
	 public void showAnswer() {
             Integer triangle = 0 , divisor = 0, maxNumber = 500;

             for(Integer number=1;number <= topNumber;number++) {
                 triangle = getTriangleNumber(number);
                 
                 divisor = printDivisors(triangle);
                 if(divisor >= maxNumber) {
                     System.out.println("\nNumber is : " + triangle + " the numbers of divisor are : " +divisor);
                     break;
                 }
             }

         } // end main
    
    /**
     * function x sub n = n(n+1)/2 ; n = 1 to N
     * @param number is a index for get the number
     * @return integer from triangle function
     */
    private Integer getTriangleNumber(Integer number) {
        Integer n = 0;
        n = number;
        n = n * (n + 1) / 2;
        System.out.println("getTriangleNumber() triangle number : " + n);
        return n;
    } //end getTriangleNumber
       
    private Integer printDivisors(Integer n) {
        Integer divisors = 0;
        // Note that this loop runs till square root
        for (Integer i=1; i<=Math.sqrt(n); i++) {
            if (n%i==0) {
                // If divisors are equal, print only one
                if (n/i == i) {
                    //System.out.printf("%d ", i);
                    divisors++;
                }
                else {
                    // Otherwise print both
                    //System.out.printf("%d %d ", i, n / i);
                    divisors = divisors + 2;
                }
            }
        }
        //System.out.println("\nDivisors : " + divisors);
        return divisors;
    } //end printDivisiors
    
}
