package euler.common;

import java.util.ArrayList;
import java.util.List;

public class Problem005 {

	public Problem005() {
	}
	
	/**
	 * Problem 5.
	 * 
	 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
	 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
	 */

	public void showAnswer() {
		List<Integer> values = getNumber(20);
		Integer numbersPrime = 1, numberComplex = 1;
		
		for (Integer i : values) {
			if(isPrime(i)) {
				numbersPrime = numbersPrime * i;
			} else { 
				numberComplex = numberComplex * getBase(i);
			}
			
		} //end for
		System.out.println(" Result : " + numbersPrime * numberComplex);
	} 
	
	public List<Integer> getNumber(Integer maxNumber) {
		List<Integer> numbers = new ArrayList<Integer>();
		for(Integer i = 1; i <= maxNumber; i++){
			numbers.add(i);
		}
		return numbers; 
	} //end getNumber
	
	public boolean isPrime(Integer number) {
		boolean isPrime = true;

		Integer auxNumber = number;
		Integer auxValue = 2;

		while (isPrime == true && auxNumber > auxValue) {
			if (auxNumber%auxValue != 0) {
				auxValue++;
			} else {
				isPrime = false;
			}
		}
		return isPrime;
	} //end isPrime

	public Integer getBase(Integer number){
		Integer base, exp = 1, auxNumber = 1;
		boolean equal = false;
		
		if(number == 0) return 1;
			
		for(base = 2; base < number && !equal; base++){
			auxNumber = 1;
			for(exp = 1; auxNumber < number && !equal; exp++){
				auxNumber = base * auxNumber;
				if(auxNumber == number) {
					equal = true;
					auxNumber = base;
				} // end if
			} //for exp
			
		} //end base
		
		if(auxNumber == 0 || base == number) auxNumber = 1 ;
		
		return auxNumber;
	}

}
